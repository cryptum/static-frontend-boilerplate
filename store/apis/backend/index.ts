import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const backendApi = createApi({
  reducerPath: "backend",
  baseQuery: fetchBaseQuery({ baseUrl: process.env.BACKEND_BASE_URL }),
  tagTypes: [],
  endpoints: () => ({}),
});
