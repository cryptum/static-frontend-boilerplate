import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "..";

export interface NotificationState {
  notifications: Notification[];
}

export interface Notification {
  type: "success" | "error" | "warning" | "info";
  message: string;
}

const initialState: NotificationState = {
  notifications: [],
};

const notificationSlice = createSlice({
  name: "notifications",
  initialState,
  reducers: {
    addNotification: (state, action: PayloadAction<Notification>) => {
      state.notifications.push(action.payload);
    },
  },
  extraReducers: (builder) => {},
});

export default notificationSlice.reducer;

export const { addNotification } = notificationSlice.actions;

export const getNotifications = (state: RootState) =>
  state.notifications.notifications;
