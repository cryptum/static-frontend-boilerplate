import { combineReducers, configureStore } from "@reduxjs/toolkit";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";
import { backendApi } from "./apis";
import { notificationsReducer } from "./ducks";

const persistConfig = {
  key: "root",
  version: 1,
  storage,
  blacklist: [backendApi.reducerPath, "notifications"],
};

const persistedReducer = persistReducer(
  persistConfig,
  combineReducers({
    [backendApi.reducerPath]: backendApi.reducer,
    notifications: notificationsReducer,
  })
);

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, PAUSE, PERSIST, REGISTER, REHYDRATE, PURGE],
      },
    }).concat(backendApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const persistor = persistStore(store);

export default store;
