import { createTheme } from "@mui/material/styles";
import { red } from "@mui/material/colors";
import { PaletteOptions } from "@mui/material";

// Create a theme instance.
const theme = createTheme({});

declare module "@mui/material/styles" {
  interface Theme {}

  interface ThemeOptions {}
}

export default theme;
