/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  inlineImageLimit: false,
  env: {
    BACKEND_BASE_URL:
      process.env.NEXT_PUBLIC_BACKEND_BASE_URL ||
      "http://localhost:8080/api/v1",
  },
  images: {
    loader: "custom",
  },
  trailingSlash: true,
}

module.exports = nextConfig
