import * as React from "react";
import Head from "next/head";
import { AppProps } from "next/app";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import theme from "../styles/theme";
import { Provider } from "react-redux";
import store, { persistor } from "../store";
import SnackbarProvider from "../components/SnackbarNotification";
import { PersistGate } from "redux-persist/integration/react";

const App = (props: AppProps) => {
  const { Component, pageProps } = props;
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <Head>
          <title>Frontend BoilerPlate</title>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <SnackbarProvider>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Component {...pageProps} />
          </ThemeProvider>
        </SnackbarProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
