## Static Frontend Boilerplate

This is a repository intended to be used as a boilerplate for static frontend projects.

It is based on the Next.js framework and uses TypeScript.

It comes pre-configured with the following features:

- [Material-UI](https://mui.com/) for styling and components
- [React-form-hook](https://react-hook-form.com/) for form validation and state management. Reference /components/formComponents for implementation alognside the MUIComponents
- [Redux](https://redux-toolkit.js.org/) for state management. Built on top of redux-toolkit for easier configuration
- [Redux-query](https://redux-toolkit.js.org/) for endpoint data fetching and mutation
- Redux-persist for localStorage persistence
- Custom Snackbar Notifications for displaying messages
- Custom ImageLoader to load images with Next's image component

## Getting Started

To use this as boilerplate, you'll need to take the following steps:

Don't fork or clone this repo! Instead, create a new, empty directory on your machine and git init (or create an empty repo on Github and clone it to your local machine)

Run the following commands:

- git remote add static-frontend-boilerplate [GIT_REPO_URL]
- git fetch static-frontend-boilerplate
- git merge static-frontend-boilerplate/main
