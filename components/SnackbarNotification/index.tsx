import React, { ReactNode, useEffect } from "react";
import { SnackbarProvider, useSnackbar } from "notistack";
import { getNotifications } from "../../store/ducks/notifications";
import { useSelector } from "react-redux";

function SnackbarNotification({ children }: { children: ReactNode }) {
  const notifications = useSelector(getNotifications);

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    if (notifications.length) {
      const lastNotification = notifications[notifications.length - 1];
      const key = enqueueSnackbar(lastNotification.message, {
        variant: lastNotification.type,
      });

      setTimeout(() => closeSnackbar(key), 5000);
    }
  }, [notifications, enqueueSnackbar, closeSnackbar]);

  return <>{children}</>;
}

const NotificationWrapper = ({ children }: { children: ReactNode }) => {
  return (
    <SnackbarProvider maxSnack={3}>
      <SnackbarNotification>{children}</SnackbarNotification>
    </SnackbarProvider>
  );
};

export default NotificationWrapper;
