import React from "react";
import { Controller } from "react-hook-form";
import { IconButton, Box } from "@mui/material";
import { FormInputProps } from "./FormInputProps";
import CameraAltOutlinedIcon from "@mui/icons-material/CameraAltOutlined";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import CancelIcon from "@mui/icons-material/Cancel";

export const FormInputImageUpload = ({
  name,
  control,
  defaultValue,
  validate,
}: FormInputProps) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange }, fieldState, formState }) => {
        return (
          <>
            <input
              accept="image/*"
              id="icon-button-file"
              type="file"
              style={{ display: "none" }}
              onChange={(e) => {
                onChange(e.target.files);
              }}
            />
            <label htmlFor="icon-button-file">
              <Box
                sx={{
                  cursor: "pointer",
                  position: "absolute",
                  bottom: "0",
                  right: "0",
                  background: "#6633CC",
                  borderRadius: "50%",
                  width: "50px",
                  height: "50px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <IconButton
                  color="primary"
                  aria-label="upload picture"
                  component="span"
                >
                  <CameraAltOutlinedIcon sx={{ color: "white" }} />
                </IconButton>
              </Box>
            </label>
          </>
        );
      }}
      defaultValue={defaultValue}
      rules={validate}
    />
  );
};
