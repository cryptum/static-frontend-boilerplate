import React, { useState } from "react";
import { Controller } from "react-hook-form";
import { OutlinedInput, TextField, TextFieldProps } from "@mui/material";
import { FormInputProps } from "./FormInputProps";
import NumberFormat from "react-number-format";
import InputAdornment from "@mui/material/InputAdornment";
import { CardFlagIcon } from "../CardFlagIcon";
import InputLabel from "@mui/material/InputLabel";

const formLabelStyles = {
  fontWeight: 600,
  fontSize: "14px",
  lineHeight: "150%",
  color: "black",
  opacity: 0.5,
  margin: "0.5em 0",
  textTransform: "uppercase",
  cursor: "pointer",
} as const;

export const FormInputText = ({
  name,
  control,
  label,
  styles,
  type,
  defaultValue,
  validate,
  placeholder,
  icon,
  mask,
  cardNumber,
  rows,
  labelName,
  labelStyles,
  format,
  disabled,
  ...rest
}: FormInputProps & TextFieldProps) => {
  const [focus, setFocus] = useState(false);

  function limit(val: string, max: string) {
    if (val.length === 1 && val[0] > max[0]) {
      val = "0" + val;
    }

    if (val.length === 2) {
      if (Number(val) === 0) {
        val = "01";
      } else if (val > max) {
        val = max;
      }
    }

    return val;
  }

  function cardExpiry(val: string) {
    let month = limit(val.substring(0, 2), "12");
    let year = val.substring(2, 6);

    return month + (year.length ? "/" + year : "");
  }

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState, formState }) => {
        return mask ? (
          <>
            {labelName && (
              <InputLabel
                htmlFor={name}
                sx={[formLabelStyles, labelStyles, focus && { opacity: 1 }]}
              >
                {labelName}
              </InputLabel>
            )}
            {mask === "decimal" && (
              <NumberFormat
                customInput={TextField}
                thousandSeparator={true}
                helperText={
                  fieldState.invalid ? fieldState?.error?.message : null
                }
                error={fieldState.invalid}
                fullWidth
                label={label}
                variant="outlined"
                sx={styles}
                onChange={onChange}
                value={value}
                placeholder={placeholder}
                allowedDecimalSeparators={[",", "."]}
                decimalScale={2}
                fixedDecimalScale
                id={name}
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
                disabled={disabled}
              />
            )}
            {mask === "creditCard" && (
              <NumberFormat
                customInput={TextField}
                helperText={
                  fieldState.invalid ? fieldState?.error?.message : null
                }
                error={fieldState.invalid}
                fullWidth
                label={label}
                variant="outlined"
                sx={styles}
                onChange={onChange}
                value={value}
                format="#### #### #### ####"
                placeholder={placeholder}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <CardFlagIcon cardNumber={cardNumber} />
                    </InputAdornment>
                  ),
                }}
                id={name}
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
              />
            )}
            {mask === "mounthYear" && (
              <NumberFormat
                customInput={TextField}
                helperText={
                  fieldState.invalid ? fieldState?.error?.message : null
                }
                error={fieldState.invalid}
                fullWidth
                label={label}
                variant="outlined"
                sx={styles}
                onChange={onChange}
                value={value}
                format={cardExpiry}
                placeholder={placeholder}
                id={name}
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
              />
            )}
            {mask === "number" && (
              <NumberFormat
                customInput={TextField}
                helperText={
                  fieldState.invalid ? fieldState?.error?.message : null
                }
                error={fieldState.invalid}
                fullWidth
                label={label}
                variant="outlined"
                sx={styles}
                onChange={onChange}
                value={value}
                placeholder={placeholder}
                id={name}
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
                format={format}
              />
            )}
          </>
        ) : icon ? (
          <>
            {labelName && (
              <InputLabel
                htmlFor={name}
                sx={[formLabelStyles, labelStyles, focus && { opacity: 1 }]}
              >
                {labelName}
              </InputLabel>
            )}
            <OutlinedInput
              startAdornment={icon.position === "start" && icon.component}
              endAdornment={icon.position === "end" && icon.component}
              error={fieldState.invalid}
              onChange={onChange}
              value={value}
              fullWidth
              label={label}
              placeholder={placeholder}
              sx={styles}
              id={name}
              onFocus={() => setFocus(true)}
              onBlur={() => setFocus(false)}
            />
          </>
        ) : (
          <>
            {labelName && (
              <InputLabel
                htmlFor={name}
                sx={[formLabelStyles, labelStyles, focus && { opacity: 1 }]}
              >
                {labelName}
              </InputLabel>
            )}
            <TextField
              helperText={
                fieldState.invalid ? fieldState?.error?.message : null
              }
              error={fieldState.invalid}
              onChange={onChange}
              value={value}
              fullWidth
              label={label}
              placeholder={placeholder}
              variant="outlined"
              sx={styles}
              multiline={rows ? true : false}
              rows={rows}
              id={name}
              onFocus={() => setFocus(true)}
              onBlur={() => setFocus(false)}
              {...rest}
            />
          </>
        );
      }}
      defaultValue={defaultValue}
      rules={validate}
    />
  );
};
