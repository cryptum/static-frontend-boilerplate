import React, { useState } from "react";
import { Controller } from "react-hook-form";
import {
  FormHelperText,
  IconButton,
  InputAdornment,
  OutlinedInput,
  TextField,
} from "@mui/material";
import { FormInputProps } from "./FormInputProps";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import InputLabel from "@mui/material/InputLabel";

const formLabelStyles = {
  fontWeight: 600,
  fontSize: "14px",
  lineHeight: "150%",
  color: "black",
  opacity: 0.5,
  margin: "0.5em 0",
  textTransform: "uppercase",
  cursor: "pointer",
} as const;

export const PasswordFormField = ({
  name,
  control,
  styles,
  defaultValue,
  validate,
  labelName,
  labelStyles,
}: FormInputProps) => {
  const [showPassword, toggleShowPassword] = useState<boolean>(false);
  const [focus, setFocus] = useState(false);

  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState, formState }) => {
        return (
          <>
            <InputLabel htmlFor={name} sx={[formLabelStyles, labelStyles, focus && {opacity: 1}]}>
              {labelName}
            </InputLabel>
            <OutlinedInput
              id={name}
              sx={styles}
              type={showPassword ? "text" : "password"}
              value={value}
              onChange={onChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => toggleShowPassword(!showPassword)}
                    onMouseDown={(e) => e.preventDefault()}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              placeholder="Password"
              error={fieldState.invalid}
              onFocus={() => setFocus(true)}
              onBlur={() => setFocus(false)}
            />
            {fieldState.invalid && (
              <FormHelperText error sx={{ paddingLeft: "15px" }}>
                {fieldState.error?.message}
              </FormHelperText>
            )}
          </>
        );
      }}
      defaultValue={defaultValue}
      rules={validate}
    />
  );
};
