import { SxProps, Theme } from "@mui/material";
import React, { ElementType, JSXElementConstructor, ReactElement } from "react";
import { ReactJSXElement } from "@emotion/react/types/jsx-namespace";
import { RegisterOptions } from "react-hook-form";

export interface FormInputProps {
  name: string;
  control: any;
  label?: string | number;
  setValue?: any;
  styles?: any;
  type?: string;
  defaultValue?: string;
  validate?: RegisterOptions;
  mask?: "decimal" | "creditCard" | "mounthYear" | "number" | "";
  placeholder?: string;
  icon?: {
    position: "start" | "end";
    component: ReactElement;
  };
  rows?: number;
  cardNumber?: any;
  labelName?: string;
  labelStyles?: any;
  acceptFiles?: any;
  format?: string;
}

export interface PropsWithFormControlLabel
  extends Omit<FormInputProps, "label"> {
  label:
    | string
    | number
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>;
}

export interface Option {
  label:
    | string
    | number
    | React.ReactElement<any, string | React.JSXElementConstructor<any>>;
  value: string | number;
  disabled?: boolean;
}

export interface PropsWithOption extends FormInputProps {
  options: Option[];
  inputStyles?: SxProps<Theme>;
  multiple?: boolean;
  selectIcon?: ElementType;
}

export interface DateFormInputProps extends FormInputProps {
  format: string;
  adornmentProps?: {
    sx: SxProps<Theme>;
  };
}
