import React, { useState } from "react";
import {
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  OutlinedInput,
  Select,
  Button,
} from "@mui/material";
import { Controller } from "react-hook-form";
import { PropsWithOption } from "./FormInputProps";

const formLabelStyles = {
  display: "block",
  textAlign: "left",
  padding: 0,
  fontWeight: 600,
  fontSize: "14px",
  lineHeight: "150%",
  color: "black",
  opacity: 0.5,
  margin: "0.5em 0",
  textTransform: "uppercase",
  cursor: "pointer",
  "&:hover": {
    background: "transparent",
  },
} as const;

export const FormInputDropdown: React.FC<PropsWithOption> = ({
  name,
  control,
  label,
  options,
  multiple,
  validate,
  placeholder,
  defaultValue,
  inputStyles,
  selectIcon,
  labelName,
  labelStyles,
}) => {
  const [open, setOpen] = useState(false);

  const generateSingleOptions = () => {
    return options.map((option: any) => {
      return (
        <MenuItem
          key={option.value}
          value={option.value}
          disabled={option.disabled}
        >
          {option.label}
        </MenuItem>
      );
    });
  };

  return (
    <FormControl fullWidth>
      {labelName && (
        <Button
          sx={[formLabelStyles, labelStyles, open && { opacity: 1 }]}
          onClick={() => setOpen(!open)}
        >
          {labelName}
        </Button>
      )}
      <Controller
        render={({
          field: { onChange, value },
          fieldState: { error, invalid },
        }) => (
          <>
            <Select
              open={open}
              onOpen={() => setOpen(true)}
              onClose={() => setOpen(false)}
              variant="outlined"
              multiple={multiple}
              onChange={onChange}
              value={value}
              input={
                <OutlinedInput
                  sx={{
                    ...inputStyles,
                    opacity: value === defaultValue ? 0.6 : 1,
                  }}
                  label={label}
                />
              }
              error={invalid}
              placeholder={placeholder}
              displayEmpty
              sx={{ ...inputStyles }}
              IconComponent={selectIcon}
            >
              <MenuItem disabled value="">
                {placeholder}
              </MenuItem>
              {generateSingleOptions()}
            </Select>
            {invalid && (
              <FormHelperText error={invalid}>{error?.message}</FormHelperText>
            )}
          </>
        )}
        control={control}
        name={name}
        rules={validate}
        defaultValue={defaultValue}
      />
    </FormControl>
  );
};
