import React from "react";
import { Controller } from "react-hook-form";
import {
  Checkbox,
  FormControlLabel,
  FormHelperText,
  TextField,
} from "@mui/material";
import { PropsWithFormControlLabel } from "./FormInputProps";
import NumberFormat from "react-number-format";

export const FormInputCheckbox = ({
  name,
  control,
  label,
  styles,
  defaultValue,
  validate,
}: PropsWithFormControlLabel) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState, formState }) => {
        return (
          <>
            <FormControlLabel
              sx={styles}
              control={
                <Checkbox onChange={onChange} checked={value} value={value} />
              }
              label={label}
            />
            {fieldState.invalid && (
              <FormHelperText error sx={{ paddingLeft: "15px" }}>
                {fieldState.error?.message}
              </FormHelperText>
            )}
          </>
        );
      }}
      defaultValue={defaultValue}
      rules={validate}
    />
  );
};
