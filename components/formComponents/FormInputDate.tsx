import React from "react";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import { Controller } from "react-hook-form";
import { DateFormInputProps } from "./FormInputProps";
import { DesktopDatePicker } from "@mui/lab";
import { TextField } from "@mui/material";

export const FormInputDate = ({
  name,
  control,
  label,
  styles,
  format,
  defaultValue,
  validate,
  adornmentProps,
}: DateFormInputProps) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue}
        render={({ field, fieldState }) => (
          <DesktopDatePicker
            label={label}
            renderInput={(params) => (
              <TextField sx={styles} {...params} error={fieldState.invalid} />
            )}
            {...field}
            inputFormat={format}
            InputAdornmentProps={adornmentProps}
          />
        )}
        rules={validate}
      />
    </LocalizationProvider>
  );
};
