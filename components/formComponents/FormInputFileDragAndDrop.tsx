import React, { useRef, useState } from "react";
import { Controller } from "react-hook-form";
import { Box, Typography } from "@mui/material";
import { FormInputProps } from "./FormInputProps";
import SearchIcon from "@mui/icons-material/Search";
import FileUploadIcon from "@mui/icons-material/FileUpload";

export const FormInputFileDragAndDrop = ({
  name,
  control,
  defaultValue,
  validate,
  acceptFiles,
}: FormInputProps) => {
  const [dragEnter, setDragEnter] = useState(false);
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange }, fieldState, formState }) => {
        return (
          <>
            <Box
              sx={{
                position: "relative",
                borderRadius: "10px",
                outline: "2px dashed #4F3295",
                outlineOffset: "-2px",
                background: dragEnter ? "#FFF" : "#F2F2F2",
                width: "100%",
              }}
            >
              <input
                name={name}
                accept={acceptFiles}
                id="icon-button-file"
                type="file"
                style={{
                  opacity: 0,
                  position: "absolute",
                  width: "100%",
                  top: 0,
                  bottom: 0,
                  left: 0,
                  right: 0,
                }}
                onChange={(e) => {
                  onChange(e.target.files);
                }}
                onDragEnter={() => setDragEnter(true)}
                onDragLeave={() => setDragEnter(false)}
                onDrop={() => setDragEnter(false)}
              />
              <Box
                sx={{
                  cursor: "pointer",
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  padding: "2rem",
                  height: "200px",
                }}
              >
                {dragEnter ? (
                  <>
                    <FileUploadIcon
                      sx={{ color: "#6633CC", fontSize: "3rem" }}
                    />
                    <Typography>Drop</Typography>
                  </>
                ) : (
                  <>
                    <Typography fontWeight={600}>
                      Drag and drop files here
                    </Typography>
                    <Typography fontWeight={600}>or</Typography>

                    <label
                      htmlFor="icon-button-file"
                      style={{
                        display: "flex",
                        background: "transparent",
                        padding: ".75rem 1.75rem",
                        marginTop: "1rem",
                        cursor: "pointer",
                        border: "2px solid #6633CC",
                        borderRadius: "100px",
                        color: "#6633CC",
                        fontWeight: 600,
                        alignItems: "center",
                        gap: ".5rem",
                        zIndex: 2,
                      }}
                    >
                      Browse File
                      <SearchIcon sx={{ color: "#6633CC" }} />
                    </label>
                  </>
                )}
              </Box>
            </Box>
          </>
        );
      }}
      defaultValue={defaultValue}
      rules={validate}
    />
  );
};
